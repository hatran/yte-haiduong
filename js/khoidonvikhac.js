var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục Dân số - Kế hoạch hoá gia đình",
   "icon": "maps/map-images/cckhhgd.png",
   "address": "356 Nguyễn Lương Bằng, Thành phố Hải Dương",
   "Longtitude": 20.8621002,
   "Latitude": 106.6828519
 },
 {
   "STT": 2,
   "Name": "Chi cục An toàn vệ sinh thực phẩm.",
   "icon": "maps/map-images/attp.png",
   "address": "Số 150 Quang Trung, Thành phố Hải Dương",
   "Longtitude": 20.9454237,
   "Latitude": 106.3329255
 },
 {
   "STT": 3,
   "Name": "Trung tâm Kiểm soát bệnh tật",
   "icon": "maps/map-images/ttytdp.png",
   "address": "Số 18 Đường Thanh Niên",
   "Longtitude": 20.9456141,
   "Latitude": 106.333757
 },
 {
   "STT": 4,
   "Name": "Trung tâm Kiểm nghiệm Thuốc-Mỹ phẩm-Thực phẩm",
   "icon": "maps/map-images/ttkn.png",
   "address": "150 Quang Trung, Thành phố Hải Dương",
   "Longtitude": 20.9459998,
   "Latitude": 106.3328719
 }
];