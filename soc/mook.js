var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Phổi Hải Dương
        Bệnh viện Phụ sản Hải Dương
        Bệnh viện Nhi Hải Dương
        Bệnh viện Phục hồi chức năng Hải Dương
        Bệnh viện Tâm thần Hải Dương
        Bệnh viện Phong Chí Linh
        Bệnh viện Trường Đại học Kỹ thuật Y tế Hải Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Phổi Hải Dương
        Bệnh viện Phụ sản Hải Dương
        Bệnh viện Nhi Hải Dương
        Bệnh viện Phục hồi chức năng Hải Dương
        Bệnh viện Tâm thần Hải Dương
        Bệnh viện Phong Chí Linh
        Bệnh viện Trường Đại học Kỹ thuật Y tế Hải Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP. Hải Dương
                H. Cẩm Giàng
                H. Gia Lộc
                H. Kim Thành
                H. Kinh Môn
                H. Nam Sách
                H. Ninh Giang
                H. Thanh Hà
                H. Bình Giang
                H. Thanh Miện
                H. Tứ Kỳ`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Phổi Hải Dương
        Bệnh viện Phụ sản Hải Dương
        Bệnh viện Nhi Hải Dương
        Bệnh viện Phục hồi chức năng Hải Dương
        Bệnh viện Tâm thần Hải Dương
        Bệnh viện Phong Chí Linh
        Bệnh viện Trường Đại học Kỹ thuật Y tế Hải Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Phổi Hải Dương
        Bệnh viện Phụ sản Hải Dương
        Bệnh viện Nhi Hải Dương
        Bệnh viện Phục hồi chức năng Hải Dương
        Bệnh viện Tâm thần Hải Dương
        Bệnh viện Phong Chí Linh
        Bệnh viện Trường Đại học Kỹ thuật Y tế Hải Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
